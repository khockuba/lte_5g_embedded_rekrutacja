#include "mongoose.h"
#include "json.h"

#define PORT "1234"
char response_buffer[512];

const char err_unknown_method[] = "Unknown method, server only supports POST...";
const char err_json[]           = "Problem when parsing JSON...";
const char err_json_out[]       = "Problem calculating output(possible overflow)...";

/**
 * @brief callback for mongoose (event handler)
 * 
 * @param connection pointer to mg_connection structure
 * @param evt current event
 * @param ptr pointer to mongoose event-specific data
 */
static void server_event(struct mg_connection *connection, int evt, void *ptr)
{
    if (evt == MG_EV_HTTP_REQUEST)
    {
        struct http_message *hm = (struct http_message *)ptr;
        printf("    got HTTP request, method %.*s\r\n", (int)hm->method.len, hm->method.p);
        
        // check if sent request is using POST method
        if (strncmp(hm->method.p, "POST", hm->method.len) == 0)
        {
            // parse incomming data
            if (json_parse(hm->body.p, hm->body.len) == 3) {
                size_t len;
                // create json response, and send it if possible
                if (json_create_response(response_buffer, &len) == 0) {
                    mg_send_head(connection, 200, len, "Content-Type: text/json");
                    mg_printf(connection, "%.*s", (int)len, response_buffer);
                } else {
                    // for some reason it was not possible to create a response and send it, fallback here
                    mg_send_head(connection, 400, strlen(err_json_out), "Content-Type: text/plain");
                    mg_printf(connection, "%.*s", (int)strlen(err_json_out), err_json_out);
                }
            } else {
                // error when parsing json
                mg_send_head(connection, 400, strlen(err_json), "Content-Type: text/plain");
                mg_printf(connection, "%.*s", (int)strlen(err_json), err_json);
            }
        } else {
            // unrecognized method
            printf("    method not recognized\r\n");
            mg_send_head(connection, 404, strlen(err_unknown_method), "Content-Type: text/plain");
            mg_printf(connection, "%.*s", (int)strlen(err_unknown_method), err_unknown_method);
        }
    }

    if (evt == MG_EV_ACCEPT)
    {
        printf("Connection accepted...\r\n");
    }

    if (evt == MG_EV_ACCEPT)
    {
        printf("Connection closed...\r\n");
    }
}

int server_handle(void)
{
    struct mg_mgr server_handler;
    struct mg_connection *server_connection;

    // Init mongoose manager
    mg_mgr_init(&server_handler, NULL);
    // Bind to port 1234
    server_connection = mg_bind(&server_handler, PORT, server_event);
    if (server_connection == 0) {
        printf("failed to bind to port 1234\r\nexiting...");
        return -1;
    }
    // attach mongoose http event handler to the server
    mg_set_protocol_http_websocket(server_connection);

    for (;;)
    {
        // mongose pool method
        mg_mgr_poll(&server_handler, 500);
    }

    mg_mgr_free(&server_handler);

    return 0;
}