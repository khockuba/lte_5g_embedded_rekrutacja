#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include "jsmn.h"

static long inputs[3] = {0};

/**
 * @brief returns pointer to a string located in buffer
 * 
 * @param buffer pointer to a buffer parsed by jsmn
 * @param token currently parsed jsmntok_t variable
 * @return char* pointer to the actual string
 */
static char* json_get_string_ptr(char *buffer, const jsmntok_t token) {
    return (buffer + token.start);
}

/**
 * @brief return string length of a json object
 * 
 * @param token jsmntok_t variable
 * @return size_t string length
 */
static size_t json_get_str_length(const jsmntok_t token) {
    return token.end - token.start;
}

/**
 * @brief if possible returns number from json child object
 * 
 * @param buffer pointer to a buffer parsed by jsmn
 * @param token jsmntok_t variable (parent of a number)
 * @param output pointer to th number
 * @return int returns -1 if failed to get a number or 0 if successfull
 */
static int json_get_token_number(char *buffer, jsmntok_t *token, long *output) {
    char *ptr_number = json_get_string_ptr(buffer, *(token + 1));
    *output = strtol(ptr_number, NULL,10);
    // sanity check 
    if (*output == LONG_MAX || *output == LONG_MIN || errno == ERANGE) {
        return -1;
    }

    return 0;
}

/**
 * @brief parse buffer as json object
 * 
 * @param buffer pointer to a buffer that should be parsed
 * @param data_len length of the buffer to parse
 * @return int returns 0 if successfull or -1 otherwise
 */
int json_parse(const char *buffer, size_t data_len) {
    jsmn_parser parser;
    jsmntok_t tokens[10];
    int parsed_inputs = 0;

    jsmn_init(&parser);

    int r = jsmn_parse(&parser, buffer, data_len, tokens, 10);
    if (r == 0) {
        printf("    found no tokens in buffer");
        return -1;
    }

    for (int i = 0; i < r; i++) {
        if (tokens[i].type == JSMN_STRING && tokens[i + 1].type == JSMN_PRIMITIVE) {
            char *p = json_get_string_ptr((char*)buffer, tokens[i]);
            if (strncmp(p, "inputA", strlen("inputA")) == 0)
            {
                if (json_get_token_number((char*)buffer, &tokens[i], &inputs[0])) {
                    // Failed parsing number
                    return -1;
                }
                parsed_inputs++;
            }
            if (strncmp(p, "inputB", strlen("inputB")) == 0)
            {
                if (json_get_token_number((char*)buffer, &tokens[i], &inputs[1])) {
                    // Failed parsing number
                    return -1;
                }
                parsed_inputs++;
            }
            if (strncmp(p, "inputC", strlen("inputC")) == 0)
            {
                if (json_get_token_number((char*)buffer, &tokens[i], &inputs[2])) {
                    // Failed parsing number
                    return -1;
                }
                parsed_inputs++;
            }
        }
    }

    return parsed_inputs;
}

/**
 * @brief creates response calculated using 3 inputs from json http request
 * 
 * @param buffer pointer to a output buffer
 * @param length length of a generated json object
 * @return int 0 if successfull, -1 otherwise
 */
int json_create_response(char *buffer, size_t *length) {
    long sum = 8 * (inputs[0] + inputs[1] + inputs[2]);
    // simple overflow check
    if (sum < inputs[0] || sum < inputs[1] || sum < inputs[2]) {
        // overflow...
        return -1;
    }
    *length = sprintf(buffer, "{\"output\":%ld}", sum);
    if (*length > 0) {
        return 0;
    }
    return -1;
}