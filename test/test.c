#include <limits.h>
#include "mongoose.h"

static const char *url = "127.0.0.1:1234";
static int exit_flag = 0;
static int tests_passed = 0;

// three test event handlers that will check proper server behaviour
static void test1(struct mg_connection *c, int ev, void *p)
{
    if (ev == MG_EV_HTTP_REPLY)
    {
        struct http_message *hm = (struct http_message *)p;
        c->flags |= MG_F_CLOSE_IMMEDIATELY;
        exit_flag = 1;

        // Check only if server handles GET correctly (return 404)
        if (hm->resp_code == 404) {
            tests_passed++;
            printf("Test passed!\r\n");
        } else {
            printf("Test failed!\r\n");
        }
    }
    else if (ev == MG_EV_CLOSE)
    {
        exit_flag = 1;
    }
}

static void test2(struct mg_connection *c, int ev, void *p)
{
    if (ev == MG_EV_HTTP_REPLY)
    {
        struct http_message *hm = (struct http_message *)p;
        c->flags |= MG_F_CLOSE_IMMEDIATELY;
        exit_flag = 1;

        // Check only if server handles GET correctly (return 404)
        if (hm->resp_code == 400) {
            tests_passed++;
            printf("Test passed!\r\n");
        } else {
            printf("Test failed!\r\n");
        }
    }
    else if (ev == MG_EV_CLOSE)
    {
        exit_flag = 1;
    }
}

static void test3(struct mg_connection *c, int ev, void *p)
{
    char response[] = "{\"output\":48}";
    if (ev == MG_EV_HTTP_REPLY)
    {
        struct http_message *hm = (struct http_message *)p;
        c->flags |= MG_F_CLOSE_IMMEDIATELY;
        exit_flag = 1;

        // Check only if server handles GET correctly (return 404)
        if (hm->resp_code == 200 && strncmp(response, hm->body.p, strlen(response)) == 0) {
            tests_passed++;
            printf("Test passed!\r\n");
        } else {
            printf("Test failed!\r\n");
        }
    }
    else if (ev == MG_EV_CLOSE)
    {
        exit_flag = 1;
    }
}

int main(int argc, char const *argv[])
{
    struct mg_mgr mgr;
    int events;

    mg_mgr_init(&mgr, NULL);
    
    // Simple GET Request Test 1
    printf("Running test 1/4...\r\n");
    if (mg_connect_http(&mgr, test1, url, NULL, NULL) == NULL) {
        printf("Failed to connect to server\r\n");
        return -1;
    }
    while (exit_flag == 0 || events)
    {
        events = mg_mgr_poll(&mgr, 1000);
    }

    // Simple POST Request Test 2
    exit_flag = 0;
    printf("Running test 2/4...\r\n");
    mg_connect_http(&mgr, test2, url, "Content-Type: text/plain\r\n", "test");
    while (exit_flag == 0 || events)
    {
        events = mg_mgr_poll(&mgr, 1000);
    }

    // Simple POST Request Test 3 (correct data)
    exit_flag = 0;
    printf("Running test 3/4...\r\n");
    mg_connect_http(&mgr, test3, url, "Content-Type: text/json\r\n", "{\"inputA\":1,\"inputB\":2,\"inputC\":3}");
    while (exit_flag == 0 || events)
    {
        events = mg_mgr_poll(&mgr, 1000);
    }

    // Simple POST Request Test 4 (correct data but will overflow long numbers)
    exit_flag = 0;
    printf("Running test 4/4...\r\n");
    char body[128];
    sprintf(body, "{\"inputA\":%ld,\"inputB\":2,\"inputC\":3}", LONG_MAX);
    mg_connect_http(&mgr, test2, url, "Content-Type: text/json\r\n", body);
    while (exit_flag == 0 || events)
    {
        events = mg_mgr_poll(&mgr, 1000);
    }

    mg_mgr_free(&mgr);

    printf("\r\nSummary: passed %d/4 tests\r\n", tests_passed);

    return 0;
}