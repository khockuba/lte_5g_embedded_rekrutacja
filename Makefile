BUILD_DIR = build
MKDIR_P   = mkdir -p
TARGET    = server.out
TEST      = test.out

SRCS := \
	Src/main.c \
	Src/server.c \
	Src/json.c \
	mongoose/mongoose.c
OBJS := $(addsuffix .o,$(basename $(SRCS)))
DEPS := $(OBJS:.o=.d)

TST_SRCS := \
	test/test.c \
	mongoose/mongoose.c
TST_OBJS := $(addsuffix .o,$(basename $(TST_SRCS)))
TST_DEPS := $(TST_OBJS:.o=.d)

INC_DIRS  := mongoose jsmn Inc
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

DEFINES := \
	-DMG_ENABLE_HTTP

CPPFLAGS ?= $(INC_FLAGS) -MMD -MP $(DEFINES) -g

$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $@ $(LOADLIBES) $(LDLIBS)

$(TEST): $(TST_OBJS)
	$(CC) $(LDFLAGS) $(TST_OBJS) -o $@ $(LOADLIBES) $(LDLIBS)

all: $(TARGET) $(TEST)

clean:
	$(RM) $(TARGET) $(OBJS) $(DEPS) $(TEST) $(TST_OBJS) $(TST_DEPS)